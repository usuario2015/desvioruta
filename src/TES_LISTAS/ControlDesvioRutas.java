package TES_LISTAS;
public class ControlDesvioRutas {

    int despliegue_punto = 300; // la nueva referencia del punto.
    String[] listaSuperiorior = new String[100];
    String[] listaInferiorior = new String[100];
    int cont = 0;

    public String[] getGeocer(String latIni, String latFin) {
        String[] result = new String[30];
        result[0] = latIni;
        int count = 1;
        for (int i = 0; i < listaSuperiorior.length; i++) {
            if (listaSuperiorior[i] != null) {
                result[count] = listaSuperiorior[i];
                count++;
            }
        }
        result[count] = latFin;
        for (int j = listaInferiorior.length - 1; j >= 0; j--) {
            if (listaInferiorior[j] != null) {
                count++;
                result[count] = listaInferiorior[j];
            }
        }
        return result;
    }

    public void clasificarPuntos(String result, double lat, double lon) {
        switch (result) {
            case "H":
                listaSuperiorior[cont] = getPosRutaHorizontalSuperior(despliegue_punto, lat, lon);
                listaInferiorior[cont] = getPosRutaHorizontalInferior(despliegue_punto, lat, lon);
                cont++;
                break;
            case "NS":
                listaSuperiorior[cont] = getPosRutaVerticalDerecho(despliegue_punto, lat, lon);
                listaInferiorior[cont] = getPosRutaVerticalIzquierdo(despliegue_punto, lat, lon);
                cont++;
                break;
            case "SN":
                listaSuperiorior[cont] = getPosRutaVerticalIzquierdo(despliegue_punto, lat, lon);
                listaInferiorior[cont] = getPosRutaVerticalDerecho(despliegue_punto, lat, lon);
                cont++;
                break;
        }
    }
// Latitud de SUR A NORTE Desminuye.
// Latitud de NORTE A SUR Aumenta.
    public String verificTipoRuta(double lat1, double lon1, double lat_sig2, double lon_sig2) {
        double num = (Math.abs(lat_sig2) - Math.abs(lat1));
//        La ruta es horizontal
        String salida = "H";
        if (num < 0) {
            //la ruta esta subiendo 
            if (num < -0.00075) {
                salida = ("SN");
            }
        } else {
            //la ruta esta bajando
            if (num > 0.00075) {
                salida = ("NS");
            }
        }
        return salida;
    }

    public double rad(double x) {
        return x * Math.PI / 180;
    }

    public double Dist(double lat1, double lon1, double lat2, double lon2) {
        double R = 6378.137;                          //Radio de la tierra en km
        double dLat = rad(lat2 - lat1);
        double dLong = rad(lon2 - lon1);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(rad(lat1)) * Math.cos(rad(lat2))
                * Math.sin(dLong / 2) * Math.sin(dLong / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;
        return d * 1000;           //Retorna en >> M
    }

    public String getPosRutaHorizontalSuperior(double metros, double lat1, double lon1) {
        double sexagesimales = metros / 1000000;
        double resultado = lat1 + sexagesimales;
        return Math.rint(resultado * 1000000) / 1000000 + ", " + lon1;
    }

    public String getPosRutaHorizontalInferior(double metros, double lat1, double lon1) {
        double sexagesimales = metros / 1000000;
        double resultado = lat1 - sexagesimales;
        return Math.rint(resultado * 1000000) / 1000000 + ", " + lon1;
    }

    public String getPosRutaVerticalDerecho(double metros, double lat1, double lon1) {
        double sexagesimales = metros / 1000000;
        double resultado = lon1 + sexagesimales;
        return lat1 + ", " + Math.rint(resultado * 1000000) / 1000000;
    }

    public String getPosRutaVerticalIzquierdo(double metros, double lat1, double lon1) {
        double sexagesimales = metros / 1000000;
        double resultado = lon1 - sexagesimales;
        return lat1 + ", " + Math.rint(resultado * 1000000) / 1000000;
    }

    public boolean pnpoly(int nvert, double[] vert_x, double[] vert_y, double latitud, double longitud) {
        int i, j;
        boolean c = false;
        for (i = 0, j = nvert - 1; i < nvert; j = i++) {
            if (((vert_y[i] > longitud) != (vert_y[j] > longitud)) && (latitud < (vert_x[j] - vert_x[i])
                    * (longitud - vert_y[i])
                    / (vert_y[j] - vert_y[i])
                    + vert_x[i])) {
                c = !c;
            }
        }
        return c;
    }

    public double[] dat_x_Lat(String[] list) {
        double x[] = new double[list.length];
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) {
                x[i] = Double.parseDouble(list[i].split(",")[0]);
            }
        }
        return x;
    }

    public double[] dat_y_Long(String[] list) {
        double y[] = new double[list.length];
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) {
                y[i] = Double.parseDouble(list[i].split(",")[1]);
            }
        }
        return y;
    }

    public static void main(String[] args) {
        double puntos[][] = {
            {-3.9958126209330094, -79.20756340154048},//1
            {-3.997356492007137, -79.20727908705686},//2
            {-3.9974876005086393, -79.2063939577539},//3
            {-3.9973859245297123, -79.20523524280333},//4
            {-3.9964975956981306, -79.20528888481937},//5
            {-3.9963798641524844, -79.2041516260328},//6
            {-3.9968186758819595, -79.20357227322938},//7
            {-3.9971504599999936, -79.2026817841803},//8
            {-3.996674191699864, -79.20211851915559},//9
            {-3.9962193286244556, -79.20149088111447},//10
            {-3.99695782, -79.19891596000002},//11
            {-3.99685079, -79.19816493999997},//12
            {-3.99783544, -79.19805765000001},//13
            {-3.99858463, -79.19799327999999},//14
            {-3.99901274, -79.19795036},//15
            {-3.99892712, -79.19709205999999},//16
            {-3.99890571, -79.19657706999999},//17
            {-4.00001879, -79.19610499999999}//18
        };
        ControlDesvioRutas obj = new ControlDesvioRutas();
        for (int i = 0; i < puntos.length - 1; i++) {
            for (int j = 0; j < puntos[i].length - 1; j++) {
                double lat = puntos[i][j];
                double lon = puntos[i][j + 1];
                double lat_sig = puntos[i + 1][j];
                double lon_sig = puntos[i + 1][j + 1];
                if (obj.Dist(lat, lon, lat_sig, lon_sig) > 85) {
                    obj.clasificarPuntos(obj.verificTipoRuta(lat, lon, lat_sig, lon_sig), lat, lon);
                    break;
                }
            }
        }
        String posIni = puntos[0][0] + "," + puntos[0][1];
        String posFini = puntos[puntos.length - 1][0] + "," + puntos[puntos.length - 1][1];
        String puntosGeocerca[] = obj.getGeocer(posIni, posFini);

        double latitud = -3.997356492007137;
        double longitud = -79.20727908705686;
        obj.dat_x_Lat(puntosGeocerca);
        boolean dentro = obj.pnpoly(puntosGeocerca.length, obj.dat_x_Lat(puntosGeocerca), obj.dat_y_Long(puntosGeocerca), latitud, longitud);
        if (dentro) {
            System.out.println("Dentro de la Geocercca");
        } else {
            System.out.println("Fuera de la Geocerca");
        }
    }
}
